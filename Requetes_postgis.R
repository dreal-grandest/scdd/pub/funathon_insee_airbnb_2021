library(sf)
install.packages("rpostgis")
library(rpostgis)

con <- dbConnect("PostgreSQL", dbname = "defaultdb", # dbname = "defaultdb" si paramêtrages base postgis inchangés
                 host = "xxx", port = 5432, # host à renseigner
                 user = "xxx", password = "xxx") #infos de connexion à renseigner

pgPostGIS(con) # test de la connexion

pgListGeom(con) # liste les objets geometriques

test <- pgGetGeom(
  con,
  name=c("public","df_paris_listings"), #nom de la table
  geom = "geometry",  #variable de géométrie
  gid = NULL,
  other.cols = TRUE, #nom des colonnes à récupérer
  clauses = , #ajout de conditions au format SQL
  boundary = NULL,
  query = NULL
) # récupérer un spatialdataframe

test2 <- dbGetQuery(con, "select * from information_schema.tables") # lister les tables
test3 <- dbGetQuery(con, "select * from df_bordeaux_reviews") # récupérer un dataframe

dbDisconnect(con)



